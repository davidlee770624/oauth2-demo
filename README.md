# oauth2-demo
為了滿足多種不用的授權機制，OAuth2定義了四種Grant Type流程。
前兩個主要是Web應用，會有Browser與redirect的溝通過程; 後兩個主要是內部Servers間或信任的App與Server的相互溝通。

1.  Authorization code: 這是一個用http redirect的流程，還有browser作用其中。

2.  Implicit: 這主要是給WebApp用的一種流程，因為Client就是Browser上的Javasript (ex. ReactJ)，所以Token會顯示在redirect URI上，Client也不用認證，做法較不安全，與Authorization Code也因此有些不同。

3.  Resource owner password credentials: Client拿Resource owner的id/pwd換取Token的機制。實作上Client需要先認證，再用Resource owner的id/pwd換取Token。

4.  Client credentials: 直接用Client在Authorization Server上註冊的id/pwd換取Token的機制。
